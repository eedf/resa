# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.contrib import admin
from .models import Account, Analytic, ThirdParty, Year


@admin.register(Year)
class YearAdmin(admin.ModelAdmin):
    list_display = ('title', 'start', 'end', 'opened')
    search_fields = ('title', )
    list_filter = ('opened', )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('number', 'title')
    search_fields = ('^number', 'title')


@admin.register(ThirdParty)
class ThirdPartyAdmin(admin.ModelAdmin):
    list_display = ('number', 'title', 'type')
    search_fields = ('=number', 'title')
    list_filter = ('type', )
    ordering = ('number', )


@admin.register(Analytic)
class AnalyticAdmin(admin.ModelAdmin):
    list_display = ('number', 'title')
    search_fields = ('=number', 'title')
