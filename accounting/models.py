# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.db import models


class Year(models.Model):
    uuid = models.UUIDField(primary_key=True)
    id = models.IntegerField(unique=True)
    title = models.CharField(verbose_name="Intitulé", max_length=100)
    start = models.DateField(verbose_name="Début")
    end = models.DateField(verbose_name="Fin")
    opened = models.BooleanField(verbose_name="Ouvert", default=False)

    sync_fields = ('id', 'title', 'start', 'end', 'opened')

    class Meta:
        verbose_name = "Exercice"
        ordering = ('start', 'end')

    def __str__(self):
        return self.title


class Account(models.Model):
    uuid = models.UUIDField(primary_key=True)
    id = models.IntegerField(unique=True)
    number = models.CharField(verbose_name="Numéro", max_length=7, unique=True)
    title = models.CharField(verbose_name="Intitulé", max_length=100)

    sync_fields = ('id', 'number', 'title')

    class Meta:
        verbose_name = "Compte"
        ordering = ('number', )

    def __str__(self):
        return "{} : {}".format(self.number, self.title)


class ThirdParty(models.Model):
    TYPE_CHOICES = (
        (0, "Client"),
        (1, "Fournisseur"),
        (2, "Salarié"),
        (3, "Autre"),
    )

    uuid = models.UUIDField(primary_key=True)
    id = models.IntegerField(unique=True)
    number = models.CharField(verbose_name="Numéro", max_length=4, unique=True)
    title = models.CharField(verbose_name="Intitulé", max_length=100)
    type = models.IntegerField(verbose_name="Type", choices=TYPE_CHOICES)

    sync_fields = ('id', 'number', 'title', 'type')

    class Meta:
        verbose_name = "Tiers"
        verbose_name_plural = "Tiers"
        ordering = ('number', )

    def __str__(self):
        return "{} : {}".format(self.number, self.title)


class Analytic(models.Model):
    uuid = models.UUIDField(primary_key=True)
    id = models.IntegerField(unique=True)
    number = models.CharField(verbose_name="Numéro", max_length=3, unique=True)
    title = models.CharField(verbose_name="Intitulé", max_length=100)

    sync_fields = ('id', 'number', 'title')

    class Meta:
        verbose_name = "Analytique"
        ordering = ('number', )

    def __str__(self):
        return "{} : {}".format(self.number, self.title)
