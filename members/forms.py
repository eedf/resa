from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from dal import autocomplete
from django import forms
from .models import Person


class MasqueradeForm(forms.Form):
    person = forms.ModelChoiceField(label="Personne", queryset=Person.objects.all(),
                                    widget=autocomplete.ModelSelect2(url='person_autocomplete'))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout('person')
