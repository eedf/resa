from django.urls import path
from . import views


urlpatterns = [
    path('oidc/fail/', views.OIDCFailView.as_view(), name='oidc_fail'),
    path('structure/autocomplete/', views.StructureAutocomplete.as_view(), name='structure_autocomplete'),
    path('person/autocomplete/', views.PersonAutocomplete.as_view(), name='person_autocomplete'),
    path('user/masquerade/', views.UserMasqueradeView.as_view(), name='user_masquerade'),
]
