FROM python:3.12 AS base
WORKDIR /code
ENV DJANGO_SETTINGS_MODULE=resa.settings.prod
ENV PYTHONUNBUFFERED=1
ENV VIRTUAL_ENV=/env
RUN python -m venv ${VIRTUAL_ENV}
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install --no-cache-dir --upgrade pip

FROM base AS build
RUN pip install --no-cache-dir sphinx sphinx-rtd-theme
COPY VERSION /code/

FROM base
COPY requirements.txt /code/
RUN pip install --no-cache-dir -r requirements.txt
COPY VERSION pyproject.toml manage.py cron.sh .flake8 .coveragerc /code/
COPY resa /code/resa
COPY members /code/members
COPY accounting /code/accounting
COPY booking /code/booking
RUN ./manage.py collectstatic --noinput
