# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.utils.deprecation import MiddlewareMixin
from cuser.middleware import CuserMiddleware as BaseCuserMiddleware


class CuserMiddleware(MiddlewareMixin, BaseCuserMiddleware):
    pass
