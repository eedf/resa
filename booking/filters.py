# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

import datetime
from django.db.models import Q
from django.forms import CheckboxSelectMultiple
from django.http import QueryDict
from django.utils.dates import MONTHS
from django.utils.timezone import now
import django_filters
from members.utils import current_season, current_year
from .forms import BookingFilterForm, BookingItemFilterForm, CotisationsForm, StatsFilterForm
from .models import Booking, BookingState, BookingItem


class BookingFilter(django_filters.FilterSet):
    year_choices = [(year, str(year)) for year in range(now().year + 2, 2015, -1)]
    year = django_filters.ChoiceFilter(label="Année", choices=year_choices, field_name='year')
    month = django_filters.ChoiceFilter(label="Mois", choices=[(m, MONTHS[m].capitalize()) for m in range(1, 13)],
                                        field_name='begin__month')
    # org_type = django_filters.ChoiceFilter(label="Type d'org°", choices=Booking.ORG_TYPE_CHOICES)
    state = django_filters.ModelMultipleChoiceFilter(label="Statut", queryset=BookingState.objects.all(),
                                                     widget=CheckboxSelectMultiple)

    class Meta:
        model = Booking
        fields = ('year', 'month', 'org_type', 'state')
        form = BookingFilterForm

    def __init__(self, data, *args, **kwargs):
        initial_query = 'state=3&state=4&state=5&state=6&state=7&state=9&state=11&year={}'.format(current_year())
        if data is None:
            data = QueryDict(initial_query)
        super().__init__(data, *args, **kwargs)

    @property
    def qs(self):
        qs = super().qs.order_by('end', 'begin')
        qs = qs.select_related('state')
        qs = qs.prefetch_related('agreements', 'payments')
        return qs


class StatsFilter(BookingFilter):
    initial_query = "state=11&state=9&state=8&state=6&year={}".format(current_year())

    class Meta:
        model = Booking
        fields = BookingFilter.Meta.fields
        form = StatsFilterForm


class BookingItemFilter(django_filters.FilterSet):
    year_choices = [(year, str(year)) for year in range(current_year() + 2, 2015, -1)]
    year = django_filters.ChoiceFilter(label="Année", choices=year_choices, field_name='begin__year')
    month = django_filters.ChoiceFilter(label="Mois", choices=[(m, MONTHS[m].capitalize()) for m in range(1, 13)],
                                        field_name='begin__month')
    org_type = django_filters.ChoiceFilter(label="Type d'org°", choices=Booking.OrgType.choices,
                                           field_name="booking__org_type")
    state = django_filters.ModelMultipleChoiceFilter(label="Statut", queryset=BookingState.objects.all(),
                                                     widget=CheckboxSelectMultiple, field_name='booking__state')

    class Meta:
        model = BookingItem
        fields = ('year', 'month', 'org_type', 'state')
        form = BookingItemFilterForm

    def __init__(self, data, *args, **kwargs):
        if data is None:
            data = QueryDict(
                'year={}&state=3&state=4&state=5&state=6&state=7&state=9&state=11'.format(current_season())
            )
        super().__init__(data, *args, **kwargs)

    @property
    def qs(self):
        qs = super().qs.select_related('booking', 'booking__state')
        return qs


class CotisationsFilter(django_filters.FilterSet):
    year_choices = [(year, "{}/{}".format(year - 1, year)) for year in range(current_season(), 2016, -1)]
    year = django_filters.ChoiceFilter(label="Année", choices=year_choices, method='filter_year')
    state = django_filters.ModelMultipleChoiceFilter(label="Statut", queryset=BookingState.objects.all(),
                                                     widget=CheckboxSelectMultiple)

    class Meta:
        model = Booking
        fields = ('year', 'org_type', 'state')
        form = CotisationsForm

    def __init__(self, data, *args, **kwargs):
        if data is None:
            data = QueryDict('state=8&state=9&state=11&year={}'.format(current_season()))
        super().__init__(data, *args, **kwargs)

    def filter_year(self, qs, name, value):
        year = int(value)
        return qs.filter(begin__gt=datetime.date(year - 1, 9, 1), begin__lte=datetime.date(year, 8, 31))

    @property
    def qs(self):
        qs = super().qs.filter(
            ~Q(org_type__in=(Booking.OrgType.BECOURS, Booking.OrgType.EEDF)),
            headcount__isnull=False
        )
        qs = qs.select_related('state')
        qs = qs.order_by('begin')
        return qs
