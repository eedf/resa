# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.urls import path
from . import views


urlpatterns = [
    path('', views.BookingListView.as_view(), name='booking_list'),
    path('booking/<int:pk>/', views.BookingDetailView.as_view(), name='booking_detail'),
    path('booking/create/', views.BookingCreateView.as_view(), name='booking_create'),
    path('booking/<int:pk>/update/', views.BookingUpdateView.as_view(), name='booking_update'),
    path('booking/<int:pk>/sale_create/', views.BookingSaleCreateView.as_view(), name='booking_sale_create'),
    path('booking/<int:booking_pk>/item/create/<int:product_pk>/', views.BookingItemCreateView.as_view(),
         name='bookingitem_create'),
    path('bookingitem/<int:pk>/update/', views.BookingItemUpdateView.as_view(), name='bookingitem_update'),
    path('bookingitem/<int:pk>/delete/', views.BookingItemDeleteView.as_view(), name='bookingitem_delete'),
    path('booking.ics', views.CalendarFeed()),
    path('agreement/create/<int:booking_pk>/', views.AgreementCreateView.as_view(), name='agreement_create'),
    path('agreement/<int:pk>/', views.AgreementDetailView.as_view(), name='agreement_detail'),
    path('agreement/<int:pk>/delete/', views.AgreementDeleteView.as_view(), name='agreement_delete'),
    path('occupancy/', views.OccupancyView.as_view(), name='occupancy'),
    path('stats/', views.StatsView.as_view(), name='stats'),
    path('cotisations/', views.CotisationsView.as_view(), name='cotisations'),
    path('product/', views.ProductListView.as_view(), name='product_list'),
    path('product/<int:pk>/', views.ProductDetailView.as_view(), name='product_detail'),
    path('product/create/', views.ProductCreateView.as_view(), name='product_create'),
    path('product/<int:pk>/update/', views.ProductUpdateView.as_view(), name='product_update'),
    path('thirdparty-autocomplete/', views.ThirdPartyAutocomplete.as_view(), name='thirdparty-autocomplete'),
]
