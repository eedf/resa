# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import timedelta
from decimal import Decimal
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Case, F, Min, Max, Sum, When, Value, Q
from django.db.models.functions import Coalesce, ExtractDay, Concat, Least
from django.urls import reverse
from members.utils import current_year, today
from phonenumber_field.modelfields import PhoneNumberField
from accounting.models import Account, Analytic, ThirdParty


class Agreement(models.Model):
    date = models.DateField(verbose_name="Date")
    order = models.IntegerField(verbose_name="Numéro d'ordre")
    odt = models.FileField(upload_to='conventions', blank=True)
    pdf = models.FileField(upload_to='conventions', blank=True)
    booking = models.ForeignKey('Booking', verbose_name="Réservation", related_name='agreements',
                                on_delete=models.PROTECT)
    deadline = models.DateField(verbose_name="Date limite", blank=True, null=True)

    class Meta:
        verbose_name = "Convention"
        get_latest_by = 'date'
        ordering = ('order', )

    def number(self):
        return "{year}-{order:03}".format(year=self.booking.year, order=self.order)
    number.short_description = "Numéro"
    number.admin_order_field = Concat('booking__year', Value('-'), 'order')

    def __str__(self):
        return self.number()


class BookingState(models.Model):
    INCOME_CHOICES = (
        (1, "Potentiel"),
        (2, "Confirmé"),
        (3, "Facturé"),
        (4, "Infirmé"),
        (5, "Annulé"),
    )
    COLOR_CHOICES = (
        ('default', "Gris"),
        ('primary', "Bleu"),
        ('success', "Vert"),
        ('info', "Cyan"),
        ('warning', "Orange"),
        ('danger', "Rouge"),
    )
    title = models.CharField(verbose_name="Intitulé", max_length=100)
    income = models.IntegerField(verbose_name="Chiffre d'affaire", choices=INCOME_CHOICES)
    color = models.CharField(verbose_name="Couleur", max_length=10, choices=COLOR_CHOICES)
    ical = models.BooleanField(verbose_name="Dans l'agenda ICal", default=False)

    class Meta:
        verbose_name = "Statut"
        ordering = ('title',)

    def __str__(self):
        return self.title


class BookingManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.annotate(begin=Min('items__begin'), end=Max('items__end'))
        qs = qs.annotate(nights=ExtractDay(F('end') - F('begin')))
        qs = qs.annotate(headcount=Sum(Case(When(items__accomodation=True, then=F('items__headcount')))))
        qs = qs.annotate(headcount_cot=Sum(Case(When(
            Q(items__accomodation=True) & Q(items__price__gt=0),
            then=F('items__headcount')
        ))))
        days = ExtractDay(F('items__end') - F('items__begin'), output_field=models.DecimalField())
        overnights = Case(When(items__accomodation=True, then=days * F('items__headcount')))
        qs = qs.annotate(overnights=Sum(overnights))
        amount = Coalesce(
            F('items__price') *
            Case(
                When(items__per_night=True, then=days),
                default=Decimal(1)
            ) *
            Case(
                When(items__per_quantity=True, then=F('items__headcount')),
                default=Decimal(1),
                output_field=models.DecimalField()
            ) * Case(
                When(items__option=True, then=Value(0)),
                default=Value(1)
            ),
            Decimal(0)
        )
        sub_amount_cot = days * F('items__headcount')
        amount_cot = Case(
            When(
                Q(items__begin__lt='2023-11-01')
                & Q(items__accomodation=True)
                & ~Q(org_type__in=(Booking.OrgType.BECOURS, Booking.OrgType.EEDF)),
                then=sub_amount_cot
            ),
            When(
                Q(items__begin__gte='2023-11-01')
                & Q(items__price__gt=0)
                & Q(items__accomodation=True)
                & Q(org_type__in=(Booking.OrgType.SF, Booking.OrgType.SE, Booking.OrgType.SCHOOL)),
                then=F('items__headcount') * Decimal(3)
            ),
            When(
                Q(items__begin__gte='2023-11-01')
                & Q(items__price__gt=0)
                & Q(items__accomodation=True)
                & Q(org_type__in=(Booking.OrgType.ASSO, Booking.OrgType.PRIVATE)),
                then=F('items__headcount') * Decimal(5)
            ),
        )
        qs = qs.annotate(amount_cot=Least(Coalesce(Sum(amount_cot), Decimal(0)), Decimal(300)))
        qs = qs.annotate(amount=Coalesce(Sum(amount), Decimal(0)) + F('amount_cot'))
        qs = qs.annotate(amount_deposit=F('amount') * F('deposit') / Decimal(100))
        return qs


class Booking(models.Model):
    class OrgType(models.IntegerChoices):
        BECOURS = 7, "Bécours"
        EEDF = 1, "EEDF"
        SF = 2, "Scouts français"
        SE = 3, "Scouts étrangers"
        ASSO = 4, "Association"
        SCHOOL = 6, "Scolaires/ALSH"
        PRIVATE = 5, "Particulier"

    title = models.CharField(verbose_name="Intitulé", max_length=100, blank=True)
    year = models.IntegerField(verbose_name="Année", default=current_year)
    org_type = models.IntegerField(verbose_name="Type d'organisation", choices=OrgType.choices, blank=True, null=True)
    structure = models.CharField(verbose_name="Structure", max_length=100, blank=True)
    contact = models.CharField(verbose_name="Contact", max_length=100, blank=True)
    stay_contact = models.CharField(verbose_name="Contact du séjour", max_length=100, blank=True)
    email = models.EmailField(verbose_name="Email", blank=True)
    tel = PhoneNumberField(verbose_name="Téléphone", blank=True)
    state = models.ForeignKey(BookingState, verbose_name="Statut", blank=True, null=True, on_delete=models.PROTECT)
    description = models.TextField(verbose_name="Description", blank=True)
    signed_agreement = models.OneToOneField(Agreement, verbose_name="N° convention signée", blank=True, null=True,
                                            related_name='signedx_booking', on_delete=models.PROTECT)
    signed_agreement_scan = models.FileField(verbose_name="Scan convention signée", upload_to='conventions_signees',
                                             blank=True)
    insurance_scan = models.FileField(verbose_name="Attestation d'assurance", upload_to='assurance', blank=True)
    invoice = models.FileField(verbose_name="Facture", upload_to='factures', blank=True)
    invoice_number = models.CharField(max_length=10, blank=True)
    preferred_place = models.CharField(verbose_name="Placement souhaité", max_length=1024, blank=True)
    wood = models.CharField(verbose_name="Bois", max_length=1024, blank=True)
    exclusivity = models.BooleanField(verbose_name="Exclusivité", default=False)
    deposit = models.IntegerField(verbose_name="Arrhes (%)", default=30,
                                  validators=[MinValueValidator(0), MaxValueValidator(100)])
    thirdparty = models.ForeignKey(ThirdParty, verbose_name="Tiers", null=True, blank=True, on_delete=models.PROTECT)

    objects = BookingManager()

    class Meta:
        verbose_name = "Réservation"
        ordering = ('-year', 'title')

    def __str__(self):
        return "{} - {}".format(self.year, self.title)

    def get_absolute_url(self):
        return reverse('booking_detail', kwargs={'pk': self.pk})

    @property
    def cot_rate(self):
        if self.org_type in (self.OrgType.SF, self.OrgType.SE, self.OrgType.SCHOOL):
            return Decimal('3.00')
        if self.org_type in (self.OrgType.ASSO, self.OrgType.PRIVATE):
            return Decimal('5.00')
        return 0

    @property
    def overdue(self):
        if self.state.income != 1:
            return False
        agreement = self.agreement
        if not agreement:
            return False
        return agreement.deadline < today()

    @property
    def tel_display(self):
        if not self.tel:
            return ''
        elif self.tel.country_code == 33:
            return self.tel.as_national
        else:
            return self.tel.as_international

    @property
    def terrain(self):
        return self.items.filter(product=1).exists()

    @property
    def village(self):
        return self.items.filter(product=2).exists()

    @property
    def agreement(self):
        agreements = list(self.agreements.all())
        if not agreements:
            return None
        agreements.sort(key=lambda agreement: agreement.date, reverse=True)
        return agreements[0]

    @property
    def payment(self):
        return sum([payment.amount for payment in self.payments.all()])

    @property
    def balance(self):
        if self.amount is None and self.payment is None:
            return None
        return (self.amount or 0) - (self.payment or 0)

    @property
    def gone(self):
        return bool(self.end) and self.end < today()

    @property
    def google_id(self):
        return 'jeito{}'.format(self.id)

    @property
    def google_repr(self):
        begin = min([date for date in self.items.values_list('begin', flat=True) if date], default=None)
        end = max([date for date in self.items.values_list('end', flat=True) if date], default=None)
        if not begin or not end:
            return None
        return {
            'id': self.google_id,
            'summary': self.title + (' ?' if self.state.income == 1 else ''),
            'start': {
                'date': begin.isoformat(),
            },
            'end': {
                'date': (end + timedelta(days=1)).isoformat(),
            },
        }

    @property
    def is_eedf(self):
        return self.org_type in (Booking.OrgType.BECOURS, Booking.OrgType.EEDF)

    def get_initial(self):
        initial = {
            'sale': {
                'title': f"Séjour {self.title} du {self.begin.strftime('%d/%m/%Y')} au {self.end.strftime('%d/%m/%Y')}"
                         f" convention {self.agreement}",
                'amount': float(self.amount),
                'thirdparty': self.thirdparty.id if self.thirdparty else None,
            },
            'transactions': [],
        }
        for item in self.items.all():
            if item.place == Place.HAMLET:
                analytic = 'HAM'
            elif item.place == Place.CAMPSITE:
                analytic = 'TER'
            else:
                analytic = 'LOC'
            if self.org_type == self.OrgType.EEDF:
                account = '7083009'
            else:
                account = '7083000'
            title = item.title
            if item.price:
                title += f" {item.price:.02f} €"
            if item.price and item.per_quantity:
                title += " ×"
            if item.per_quantity and item.headcount:
                title += f" {item.headcount} {item.pretty_unit}"
            if (item.price or item.per_quantity) and item.per_night:
                title += " ×"
            if item.per_night:
                title += f" {item.nights} nuits"
                # title += f" (du {item.begin.strftime('%d/%m/%Y')} au {item.end.strftime('%d/%m/%Y')})"
            initial['transactions'].append({
                'title': title,
                'amount': float(item.amount),
                'account': Account.objects.get(number=account).id,
                'analytic': Analytic.objects.get(number=analytic).id,
            })
        if self.cot_rate:
            amount = min(self.cot_rate * self.headcount_cot, 300)
            title = "Cotisation"
            if amount < 300:
                title += f" {self.cot_rate} € × {self.headcount_cot} personnes"
            initial['transactions'].append({
                'title': title,
                'amount': float(amount),
                'account': Account.objects.get(number='7562000').id,
                'analytic': Analytic.objects.get(number='ASS').id,
            })
        return initial


class BookingItemManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.annotate(nights=ExtractDay(F('end') - F('begin'), output_field=models.IntegerField()))
        qs = qs.annotate(overnights=F('nights') * F('headcount'))
        amount = Coalesce(
            F('price') *
            Case(
                When(per_night=True, then=F('nights')),
                default=Decimal(1),
                output_field=models.DecimalField()
            ) *
            Case(
                When(per_quantity=True, then=F('headcount')),
                default=Decimal(1),
                output_field=models.DecimalField()
            ),
            Decimal(0)
        )
        qs = qs.annotate(amount=amount)
        return qs


class Unit(models.IntegerChoices):
    UNIT = 1, ""
    CHALET = 6, "chalet"
    COPY = 8, "copie"
    KG = 3, "kg"
    KIT = 7, "kit"
    KWH = 4, "kWh"
    PERSON = 2, "personne"
    TENT = 5, "tente"
    DAY = 9, "journée"
    HALF_DAY = 10, "demi-journée"


class Place(models.IntegerChoices):
    HAMLET = 1, "Hameau"
    CAMPSITE = 2, "Terrain"


class Product(models.Model):
    title = models.CharField(verbose_name="Intitulé", max_length=100, blank=True)
    description = models.TextField(verbose_name="Description", blank=True)
    price = models.DecimalField(verbose_name="Tarif", max_digits=8, decimal_places=2, default=0)
    price_scout = models.DecimalField(verbose_name="Tarif scout", max_digits=8, decimal_places=2, default=0)
    price_eedf = models.DecimalField(verbose_name="Tarif EEDF", max_digits=8, decimal_places=2, default=0)
    per_night = models.BooleanField(verbose_name="Par nuit", default=False)
    per_quantity = models.BooleanField(verbose_name="Par quantité", default=True)
    accomodation = models.BooleanField(verbose_name="Hébergement", default=False)
    capacity = models.IntegerField(verbose_name="Nombre max", null=True, blank=True)
    unit = models.IntegerField(verbose_name="Unité", choices=Unit.choices, default=Unit.UNIT)
    place = models.IntegerField(verbose_name="Emplacement", choices=Place.choices, null=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('product_detail', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = "Produit"


class BookingItem(models.Model):
    booking = models.ForeignKey(Booking, related_name='items', on_delete=models.CASCADE)
    title = models.CharField(verbose_name="Intitulé", max_length=100)
    description = models.TextField(verbose_name="Description", blank=True)
    headcount = models.PositiveIntegerField(verbose_name="Nombre", blank=True, null=True)
    begin = models.DateField(verbose_name="Date de début", blank=True, null=True)
    end = models.DateField(verbose_name="Date de fin", blank=True, null=True)
    price = models.DecimalField(verbose_name="Prix", max_digits=8, decimal_places=2, null=True, blank=True)
    per_night = models.BooleanField(verbose_name="Par nuit", default=False)
    per_quantity = models.BooleanField(verbose_name="Par quantité", default=True)
    accomodation = models.BooleanField(verbose_name="Hébergement", default=False)
    unit = models.IntegerField(verbose_name="Unité", choices=Unit.choices, default=Unit.UNIT)
    place = models.IntegerField(verbose_name="Emplacement", choices=Place.choices, null=True, blank=True)
    option = models.BooleanField(verbose_name="Option", default=False)

    objects = BookingItemManager()

    def __str__(self):
        return self.title or str(self.product)

    @property
    def pretty_unit(self):
        if not self.headcount or self.headcount <= 1 or self.unit in (Unit.UNIT, Unit.KG, Unit.KWH):
            return self.get_unit_display()
        return f"{self.get_unit_display()}s"


class Payment(models.Model):
    MEAN_CHOICES = (
        (1, "Chèque"),
        (2, "Virement"),
        (3, "Espèces"),
        (4, "ANCV"),
        (5, "CB"),
    )
    mean = models.IntegerField(verbose_name="Moyen de paiement", choices=MEAN_CHOICES)
    date = models.DateField()
    amount = models.DecimalField(verbose_name="Montant", max_digits=8, decimal_places=2)
    booking = models.ForeignKey(Booking, related_name='payments', on_delete=models.CASCADE)
    scan = models.FileField(verbose_name="Scan", upload_to='paiements', blank=True)

    class Meta:
        verbose_name = "Paiement"
