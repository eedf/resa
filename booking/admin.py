# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.contrib import admin
from .models import BookingState, Booking, BookingItem, Agreement, Payment, Product


@admin.register(BookingState)
class BookingStateAdmin(admin.ModelAdmin):
    list_display = ('title', 'color', 'income', 'ical')
    list_filter = ('ical', )


@admin.register(Agreement)
class AgreementAdmin(admin.ModelAdmin):
    list_display = ('number', 'date', 'booking')
    list_filter = ('booking__year', )
    ordering = ('-booking__year', '-order', )
    date_hierarchy = 'date'


class AgreementInline(admin.TabularInline):
    model = Agreement
    fields = ('date', 'order', 'odt', 'pdf')
    extra = 1


class BookingItemInline(admin.TabularInline):
    model = BookingItem
    fields = ('product', 'title', 'headcount', 'begin', 'end', 'price', 'per_night', 'accomodation')


class PaymentInline(admin.TabularInline):
    model = Payment
    fields = ('mean', 'date', 'amount', 'scan')
    extra = 1


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    inlines = (AgreementInline, BookingItemInline, PaymentInline)
    list_display = ('title', 'year', 'state', 'contact', 'email', 'tel', 'agreement')
    list_filter = ('state', 'year')
    search_fields = ('title', 'contact', 'email')


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('mean', 'date', 'amount', 'booking')
    search_fields = ('booking__title', )
    list_filter = ('mean', )
    date_hierarchy = 'date'


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'price', 'price_scout', 'price_eedf', 'per_night', 'accomodation', 'capacity')
    search_fields = ('title', )
    list_filter = ('per_night', 'accomodation')
