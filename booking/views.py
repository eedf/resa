# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import timedelta as timedelta

from dal import autocomplete
from functools import reduce
import json
from operator import and_
from os.path import join
from weasyprint import HTML
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Sum, Min, Max, Case, When, Q, F
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import Context, Template
from django.template.loader import get_template
from django.urls import reverse
from django.utils.dates import MONTHS
from django.utils.timezone import now
from django.views.generic import TemplateView, DetailView, CreateView, ListView, UpdateView, DeleteView
from django_ical.views import ICalFeed
from django_filters.views import FilterView
from .filters import BookingFilter, BookingItemFilter, StatsFilter, CotisationsFilter
from .forms import BookingForm, ProductForm, BookingItemForm, AgreementForm
from .models import Booking, BookingItem, Agreement, Product, BookingState, Place
from accounting.models import ThirdParty, Year


class ThirdPartyAutocomplete(PermissionRequiredMixin, autocomplete.Select2QuerySetView):
    permission_required = 'booking.view_booking'

    def get_queryset(self):
        qs = ThirdParty.objects.all()
        if self.q:
            tokens = self.q.split()
            qs = qs.filter(reduce(and_, (
                Q(title__unaccent__icontains=token) | Q(number__istartswith=token)
                for token in tokens
            )))
        return qs


class BookingListView(PermissionRequiredMixin, FilterView):
    template_name = 'booking/booking_list.html'
    filterset_class = BookingFilter
    model = Booking
    permission_required = 'booking.view_booking'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['overnights'] = sum([booking.overnights for booking in self.object_list if booking.overnights])
        context['headcount'] = sum([booking.headcount for booking in self.object_list if booking.headcount])
        context['amount'] = sum([booking.amount for booking in self.object_list if booking.amount])
        context['balance'] = sum([booking.balance for booking in self.object_list if booking.balance])
        context['overnights_ext'] = sum([
            booking.overnights for booking in self.object_list
            if booking.overnights and booking.org_type != Booking.OrgType.BECOURS
        ])
        context['headcount_ext'] = sum([
            booking.headcount for booking in self.object_list
            if booking.headcount and booking.org_type != Booking.OrgType.BECOURS
        ])
        context['amount_ext'] = sum([
            booking.amount for booking in self.object_list
            if booking.amount and booking.org_type != Booking.OrgType.BECOURS
        ])
        context['balance_ext'] = sum([
            booking.balance for booking in self.object_list
            if booking.balance and booking.org_type != Booking.OrgType.BECOURS
        ])
        return context


class BookingDetailView(PermissionRequiredMixin, DetailView):
    model = Booking
    permission_required = 'booking.view_booking'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['items'] = self.object.items.order_by('-amount')
        context['products'] = Product.objects.order_by('title')
        return context


class BookingCreateView(PermissionRequiredMixin, CreateView):
    model = Booking
    form_class = BookingForm
    permission_required = 'booking.add_booking'

    def get_initial(self):
        state, _ = BookingState.objects.get_or_create(
            title='Contact',
            defaults={
                'color': 'info',
                'income': 1,
            }
        )
        return {
            'state': state,
        }


class BookingSaleCreateView(PermissionRequiredMixin, DetailView):
    model = Booking
    permission_required = 'booking.view_booking'

    def render_to_response(self, context, **response_kwargs):
        try:
            year = Year.objects.get(start__lte=self.object.end, end__gte=self.object.end)
        except Year.DoesNotExist:
            messages.error(self.request, "Erreur : l'exercice comptable n'est pas ouvert.", extra_tags="alert-danger")
            return HttpResponseRedirect(reverse('booking_detail', args=[self.object.pk]))
        initial = json.dumps(self.object.get_initial())
        return HttpResponseRedirect(
            f'{settings.ACCOUNTING_HOST}/{year.id}/sale/create/?initial={initial}',
        )


class BookingUpdateView(PermissionRequiredMixin, UpdateView):
    model = Booking
    form_class = BookingForm
    permission_required = 'booking.change_booking'


class BookingItemCreateView(PermissionRequiredMixin, CreateView):
    model = BookingItem
    form_class = BookingItemForm
    permission_required = 'booking.add_bookingitem'

    def dispatch(self, request, booking_pk, product_pk):
        self.booking = get_object_or_404(Booking, pk=booking_pk)
        self.product = get_object_or_404(Product, pk=product_pk)
        return super().dispatch(request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['booking'] = self.booking
        context['product'] = self.product
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['booking'] = self.booking
        return kwargs

    def get_initial(self):
        if self.booking.org_type in (Booking.OrgType.BECOURS, Booking.OrgType.EEDF):
            price = self.product.price_eedf
        elif self.booking.org_type in (Booking.OrgType.SF, Booking.OrgType.SE):
            price = self.product.price_scout
        else:
            price = self.product.price
        return {
            'title': self.product.title,
            'description': self.product.description,
            'headcount': min(
                self.booking.headcount or 1,
                self.product.capacity or 1
            ) if self.product.accomodation else 1,
            'price': price,
            'per_night': self.product.per_night,
            'per_quantity': self.product.per_quantity,
            'begin': self.booking.begin,
            'end': self.booking.end,
            'accomodation': self.product.accomodation,
            'unit': self.product.unit,
            'place': self.product.place,
        }

    def form_valid(self, form):
        item = form.save(commit=False)
        item.booking = self.booking
        item.product = self.product
        self.object = item.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('booking_detail', args=[self.booking.pk])


class BookingItemUpdateView(PermissionRequiredMixin, UpdateView):
    model = BookingItem
    form_class = BookingItemForm
    permission_required = 'booking.change_bookingitem'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['booking'] = self.object.booking
        return kwargs

    def get_success_url(self):
        return reverse('booking_detail', args=[self.object.booking.pk])


class BookingItemDeleteView(PermissionRequiredMixin, DeleteView):
    model = BookingItem
    permission_required = 'booking.delete_bookingitem'

    def get_success_url(self):
        return reverse('booking_detail', args=[self.object.booking.pk])


class AgreementDetailView(PermissionRequiredMixin, DetailView):
    model = Agreement
    template_name = 'booking/agreement.html'
    permission_required = 'booking.view_agreement'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['booking'] = Booking.objects.get(id=self.object.booking_id)
        return context


class AgreementCreateView(PermissionRequiredMixin, CreateView):
    model = Agreement
    form_class = AgreementForm
    permission_required = 'booking.add_agreement'

    def dispatch(self, request, booking_pk):
        self.booking = get_object_or_404(Booking, pk=booking_pk)
        return super().dispatch(request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['booking'] = self.booking
        return context

    def get_initial(self):
        return {
            'date': now(),
            'deadline': now() + timedelta(days=14),
        }

    def form_valid(self, form):
        agreements = Agreement.objects.filter(booking__year=self.booking.year)
        order = agreements.aggregate(Max('order'))['order__max']
        order = (order or 0) + 1
        agreement = form.save(commit=False)
        agreement.booking = self.booking
        agreement.order = order
        agreement.pdf = f'conventions/convention-{self.booking.year}-{order:03d}.pdf'
        template = get_template('booking/agreement.html')
        items = self.booking.items.order_by('-amount')
        for item in items:
            context = Context({'item': item})
            item.description = Template(item.description).render(context)
        markup = template.render({
            'booking': self.booking,
            'agreement': agreement,
            'items': items,
            'signatory': settings.BOOKING_SIGNATORY,
        })
        filename = join(settings.MEDIA_ROOT, agreement.pdf.name)
        HTML(string=markup, base_url=str(settings.BASE_DIR)).write_pdf(filename)
        agreement.save()
        return HttpResponseRedirect(reverse('booking_detail', args=[self.booking.pk]))


class AgreementDeleteView(PermissionRequiredMixin, DeleteView):
    model = Agreement
    permission_required = 'booking.delete_agreement'

    def get_success_url(self):
        return reverse('booking_detail', args=[self.object.booking.pk])


class OccupancyView(PermissionRequiredMixin, FilterView):
    template_name = 'booking/occupancy.html'
    filterset_class = BookingItemFilter
    permission_required = 'booking.view_booking'

    def occupancy_for(self, day, place):
        items = self.object_list.filter(begin__lte=day, end__gt=day, place=place)
        items = items.order_by('booking__title')
        items = items.values('booking__title', 'booking__state__color')
        items = items.annotate(headcount=Sum(Case(When(Q(accomodation=True), then=F('headcount')), default=0)))
        return len(items), sum([item['headcount'] or 0 for item in items]), items

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        occupancy = []
        range_qs = self.object_list.aggregate(begin=Min('begin'), end=Max('end'))
        if range_qs['begin'] and range_qs['end']:
            begin = range_qs['begin']
            end = range_qs['end']
            for i in range((end - begin).days + 1):
                day = begin + timedelta(days=i)
                occupancy.append(
                    (day, ) +
                    self.occupancy_for(day, Place.CAMPSITE) +
                    self.occupancy_for(day, Place.HAMLET)
                )
        context['occupancy'] = occupancy
        return context


class StatsView(PermissionRequiredMixin, TemplateView):
    template_name = 'booking/stats.html'
    permission_required = 'booking.view_booking'

    def get_context_data(self, **kwargs):
        filter = StatsFilter(self.request.GET or None, request=self.request,
                             queryset=Booking.objects.all())
        items = BookingItem.objects.filter(booking__in=filter.qs, accomodation=True)
        kwargs['filter'] = filter
        kwargs['stats'] = {
            'headcount': sum([item.headcount for item in items if item.headcount]),
            'overnights': sum([item.overnights for item in items if item.overnights]),
            'amount': sum([item.amount for item in items]),
        }
        if kwargs['stats']['overnights']:
            kwargs['stats']['overnight_cost'] = kwargs['stats']['amount'] / kwargs['stats']['overnights']

        stats = [
            ('Village', items.filter(place=Place.HAMLET)),
            ('Terrain', items.filter(place=Place.CAMPSITE)),
        ]
        stats += [(MONTHS[month].capitalize(), items.filter(begin__month=month)) for month in range(1, 13)]
        kwargs['detailed_stats'] = {}
        for (name, subitems) in stats:
            substats = {
                'headcount': sum([item.headcount for item in subitems if item.headcount]),
                'overnights': sum([item.overnights for item in subitems if item.overnights]),
                'amount': sum([item.amount for item in subitems]),
            }
            if substats['overnights']:
                substats['overnights_rate'] = (100 * substats['overnights'] / kwargs['stats']['overnights'])
            if substats['amount']:
                substats['amount_rate'] = (100 * substats['amount'] / kwargs['stats']['amount'])
            if substats['overnights']:
                substats['overnight_cost'] = (substats['amount'] / substats['overnights'])

            kwargs['detailed_stats'][name] = substats

        return kwargs


class CotisationsView(PermissionRequiredMixin, FilterView):
    template_name = 'booking/cotisations.html'
    filterset_class = CotisationsFilter
    permission_required = 'booking.view_booking'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'year' in self.filterset.data:
            year = int(self.filterset.data['year'])
            context['date'] = "{}/{}".format(year, year + 1)
        context['headcount'] = sum(item.headcount for item in self.object_list)
        context['amount_cot'] = sum(item.amount_cot for item in self.object_list)
        return context


class ProductListView(PermissionRequiredMixin, ListView):
    model = Product
    permission_required = 'booking.view_product'


class ProductDetailView(PermissionRequiredMixin, DetailView):
    model = Product
    permission_required = 'booking.view_product'


class ProductCreateView(PermissionRequiredMixin, CreateView):
    model = Product
    form_class = ProductForm
    permission_required = 'booking.add_product'


class ProductUpdateView(PermissionRequiredMixin, UpdateView):
    model = Product
    form_class = ProductForm
    permission_required = 'booking.change_product'


class CalendarFeed(ICalFeed):
    title = "Réservations Bécours"
    product_id = '-//jeito.eedf.fr//reservations-becours//FR'
    timezone = settings.TIME_ZONE

    def items(self):
        return Booking.objects.filter(state__ical=True, begin__isnull=False).order_by('-begin')

    def item_guid(self, item):
        return f"booking-{item.id}@jeito.eedf.fr"

    def item_title(self, item):
        title = item.title
        if item.state.income == 1:
            title += " ???"
        return title

    def item_description(self, item):
        url = self.request.build_absolute_uri(reverse('booking_detail', args=[item.pk]))
        return f"Statut : {item.state.title}\nDétails : {url}"

    def item_start_datetime(self, item):
        return item.begin

    def item_end_datetime(self, item):
        # ICal specifies end date is exclusive
        return item.end + timedelta(days=1)

    def get_feed(self, obj, request):
        self.request = request
        return super().get_feed(obj, request)
