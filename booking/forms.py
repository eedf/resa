# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Résa.
#
# Résa is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from crispy_forms.bootstrap import InlineCheckboxes
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML
from dal import autocomplete
from django import forms
from django.utils.timezone import now
from .models import Booking, Product, BookingItem, Agreement, Unit


# TODO: add a reinit button
# TODO: use https://behigh.github.io/bootstrap_dropdowns_enhancement
class BookingFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['year'].initial = now().year
        self.helper = FormHelper()
        self.helper.attrs = {'id': 'filter'}
        self.helper.form_class = 'form-inline'
        self.helper.form_method = 'GET'
        self.helper.form_action = 'booking_list'
        self.helper.layout = Layout(
            'year',
            'month',
            'org_type',
            InlineCheckboxes('state'),
        )


class BookingItemFilterForm(BookingFilterForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.form_action = 'occupancy'


class StatsFilterForm(BookingFilterForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.form_action = 'stats'


class CotisationsForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['year'].initial = now().year
        self.helper = FormHelper()
        self.helper.attrs = {'id': 'filter'}
        self.helper.form_class = 'form-inline'
        self.helper.form_method = 'GET'
        self.helper.form_action = 'cotisations'
        self.helper.layout = Layout(
            'year',
            'org_type',
            InlineCheckboxes('state'),
        )


class BookingForm(forms.ModelForm):
    class Meta:
        model = Booking
        fields = ('title', 'org_type', 'contact', 'stay_contact', 'email', 'tel', 'state', 'description', 'year',
                  'structure', 'deposit', 'signed_agreement_scan', 'exclusivity', 'thirdparty')
        widgets = {
            'thirdparty': autocomplete.ModelSelect2(url='thirdparty-autocomplete'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-4">'),
            'title',
            'year',
            'state',
            'stay_contact',
            'signed_agreement_scan',
            HTML('</div><div class="col-md-4">'),
            'org_type',
            'structure',
            'deposit',
            'exclusivity',
            HTML('</div><div class="col-md-4">'),
            'contact',
            'email',
            'tel',
            'thirdparty',
            HTML('</div></div>'),
            'description',
        )


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ('title', 'description', 'price', 'price_scout', 'price_eedf', 'unit',
                  'per_night', 'per_quantity', 'accomodation', 'place', 'capacity')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'title',
            HTML('<div class="row"><div class="col-md-4">'),
            'price',
            'accomodation',
            'unit',
            HTML('</div><div class="col-md-4">'),
            'price_scout',
            'per_night',
            'place',
            HTML('</div><div class="col-md-4">'),
            'price_eedf',
            'per_quantity',
            'capacity',
            HTML('</div></div>'),
            'description',
        )


class BookingItemForm(forms.ModelForm):
    class Meta:
        model = BookingItem
        fields = ('title', 'description', 'headcount', 'price', 'per_night',
                  'per_quantity', 'accomodation', 'begin', 'end', 'option',
                  'unit', 'place')

    def __init__(self, booking, *args, **kwargs):
        self.booking = booking
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'title',
            HTML('<div class="row"><div class="col-md-2">'),
            'headcount',
            HTML('</div><div class="col-md-2">'),
            'begin',
            HTML('</div><div class="col-md-2">'),
            'end',
            HTML('</div><div class="col-md-2">'),
            'price',
            HTML('</div><div class="col-md-2">'),
            'unit',
            HTML('</div><div class="col-md-2">'),
            'place',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-3">'),
            'per_night',
            HTML('</div><div class="col-md-3">'),
            'per_quantity',
            HTML('</div><div class="col-md-3">'),
            'accomodation',
            HTML('</div><div class="col-md-3">'),
            'option',
            HTML('</div></div>'),
            'description',
        )

    def clean_unit(self):
        unit = self.cleaned_data['unit']
        per_quantity = self.cleaned_data.get('per_quantity')
        if per_quantity is None:
            return unit
        if per_quantity and unit == Unit.UNIT:
            raise forms.ValidationError("Merci de choisir une unité.")
        if not per_quantity:
            return Unit.UNIT
        return unit

    def clean_place(self):
        place = self.cleaned_data['place']
        accomodation = self.cleaned_data.get('accomodation')
        if accomodation is None:
            return place
        if accomodation and not place:
            raise forms.ValidationError("Merci de choisir un emplacement.")
        if not accomodation:
            return None
        return place

    def clean(self):
        cleaned_data = super().clean()
        begin = cleaned_data.get('begin')
        end = cleaned_data.get('end')
        if begin and begin.year != self.booking.year:
            raise forms.ValidationError("La réservation doit débuter en {}".format(self.booking.year))
        if end and end.year not in (self.booking.year, self.booking.year + 1):
            raise forms.ValidationError(
                "La réservation doit finir en {} ou en {}".format(self.booking.year, self.booking.year + 1)
            )
        return cleaned_data


class AgreementForm(forms.ModelForm):
    class Meta:
        model = Agreement
        fields = ('date', 'deadline')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-6">'),
            'date',
            HTML('</div><div class="col-md-6">'),
            'deadline',
            HTML('</div></div>'),
        )
