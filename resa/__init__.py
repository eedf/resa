# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from pathlib import Path

try:
    __version__ = Path(__file__).parent.parent.joinpath('VERSION').read_text()
except FileNotFoundError:
    __version__ = "unknown"

__all__ = ('__version__', )
